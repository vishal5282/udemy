import axios from "axios";

import {setAlert} from "./alert";


import {
    CLEAR_PROFILE,
    DELETE_ACCOUNT,
    GET_PROFILE,
    PROFILE_ERROR,
    UPDATE_PROFILE,
    GET_PROFILES
} from "./types";


//  GET current profile


export const getCurrentProfile = ( ) => async (dispatch) =>{

    try{

        const res = await axios.get("api/profile/me");

        dispatch({
            type: GET_PROFILE,
            payload:res.data
        });

    }catch(err){

        dispatch({ type: CLEAR_PROFILE });

        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        });
    }
};

// ---------- Get ALL profiles -----------------------------------------

export const getProfiles = () => async (dispatch) =>{

    try{

        const res = await axios.get("api/profile");

        dispatch({
            type: GET_PROFILES,
            payload:res.data
        });

    }catch(err){

        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        });
    }
};



// ---------- Get profile by id -----------------------------------------

export const getProfileById = (userId) => async (dispatch) => {
    
    try {

      const res = await axios.get(`/api/profile/user/${userId}`);
  
      dispatch({
        type: GET_PROFILE,
        payload: res.data
      });
      
    } catch (err) {
      dispatch({
        type: PROFILE_ERROR,
        payload: { msg: err.response.statusText, status: err.response.status }
      });
    }
  };

// Create and Update Profile


export const createProfile = (formData,history,edit=false) => async (dispatch) =>{

    try{

        const config = {

            "Content-Type":"apllication/json"
        }


        const res =await axios.post("api/profile",formData,config);


        dispatch({
            type:GET_PROFILE,
            payload:res.data
        });

        dispatch(setAlert(edit ? "Profile update" : "Profile Created","success"));


        if(edit){
            history.push("/dashboard");
        }

    }catch(err){

        const errors = err.response.data.errors;

        if (errors) {
          errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:err.response.statusText, status:err.response.status}
        });
    }
}



//---------------------------------  Add Experience  ---------------------------------------//

export const addExperience = (formData,history) => async (dispatch) =>{

    try{

        const config = {

            "Content-Type":"apllication/json"
        }


        const res =await axios.put("api/profile/experience",formData,config);


        dispatch({
            type:UPDATE_PROFILE,
            payload:res.data
        });

        dispatch(setAlert("experience added","success"));

            history.push("/dashboard");


    }catch(err){

        const errors = err.response.data.errors;

        if (errors) {
          errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:err.response.statusText, status:err.response.status}
        });
    }

}


//---------------------------------- Add Edeucation ------------------------------------//


export const addEducation = (formData,history) => async (dispatch) =>{

    try{

        const config = {

            "Content-Type":"apllication/json"
        }


        const res =await axios.put("api/profile/education",formData,config);


        dispatch({
            type:UPDATE_PROFILE,
            payload:res.data
        });

        dispatch(setAlert("education added","success"));

        history.push("/dashboard");


    }catch(err){

        const errors = err.response.data.errors;

        if (errors) {
          errors.forEach(error => dispatch(setAlert(error.msg, 'danger')));
        }

        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:err.response.statusText, status:err.response.status}
        });
    }

}


// ----------------------------------- Delete Experience -------------------------------------


export const deleteExpericence = id => async(dispatch) =>{
    try{

        const res= await axios.delete(`/api/profile/experience/${id}`);

        dispatch({
            type:UPDATE_PROFILE,
            payload:res.data
        });

        dispatch(setAlert("Experience Deleted","danger"));


    }catch(err){
       console.log(err.response);

       dispatch({
           type:PROFILE_ERROR,
           payload:{msg:err.response.statusText,status:err.response.status}
       });

    }
} 




// -------------------------------------- Delete Education ------------------------------------

export const deleteEducation = id => async(dispatch) =>{
    try{
        const res= await axios.delete(`/api/profile/education/${id}`);

        dispatch({
            type:UPDATE_PROFILE,
            payload:res.data
        });

        dispatch(setAlert("Education Deleted","danger"));

    }catch(err){
        console.log(err.response);

        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:err.response.statusText,status:err.response.status}
        });

    }
}



//  -------------------------------------- Delete Account & Profile  ---------------------------------------

export const deleteAccount = () => async(dispatch) =>{
    
    if(window.confirm("Are you Sure? This can not be Recycle.!!")){

        try{

            await axios.delete(`/api/profile`);

            dispatch({ type:CLEAR_PROFILE });
            dispatch({ type:DELETE_ACCOUNT });

            dispatch(setAlert("Your Account is permanantly Deleted","danger"));

        }catch(err){
            dispatch({
                type:PROFILE_ERROR,
                payload:{msg:err.response.statusText,status:err.response.status}
            });
        }
    } 
}




