import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Fragment } from 'react';
// import formatDate from '../../utils/formateDate';
import { withRouter } from 'react-router';
import Moment from "react-moment";
import {deleteExpericence} from "../../actions/profile";


const Experience = ({experience,deleteExpericence}) => {

    const experiences = experience.map((exp) =>(
        <tr key={exp._id}>
          <td>{exp.company}</td>
          <td className="hide-sm">{exp.title}</td>
            <td><Moment format="YYYY/MM/DD">{exp.from}</Moment>{" "}-{" "}{exp.to === null ? ("NOW"):(<Moment format="YYYY/MM/DD">{exp.to}</Moment>)}</td>
          {/* {formatDate(exp.form)}-{exp.to === null ? "Now" : (formatDate (exp.to))} */}
          <td>
              <button onClick={() =>deleteExpericence(exp._id)} className="btn btn-danger">Delete</button>
          </td>
        </tr>
    ));
    
    return (
        <Fragment>
            <h2 className="my-2">Experience Credentials</h2>
            <table className="table">
            <thead>
                <tr>
                    <th>Company</th>
                    <th className="hide-sm">Title</th>
                    <th className="hide-sm">Years</th>
                    <th className="hid-sm">Delete</th>
                </tr>
            </thead>
            <tbody>
                {
                    experiences
                }
            </tbody>
            </table>
        </Fragment>
    )
}

Experience.propTypes = {
    experience:PropTypes.array.isRequired,
    deleteExpericence:PropTypes.func.isRequired
}



export default connect(null,{deleteExpericence})(withRouter(Experience));
