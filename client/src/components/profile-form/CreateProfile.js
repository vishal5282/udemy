import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {createProfile} from "../../actions/profile";
import {Link, withRouter} from 'react-router-dom';


function CreateProfile({createProfile,history}) {
  const [formData, setformData] = useState({
    company: "",
    website: "",
    location: "",
    status: "",
    skills: "",
    githubusername: "",
    bio: "",
    youtube: "",
    twitter: "",
    facebook: "",
    linkedin: "",
    instagram: "",
  });


  const [displaySocialInputs,toggleSocialInputs] = useState(false);

  const {
      company,
      website,
      location,
      status,
      skills,
      githubusername,
      bio,
      youtube,
      twitter,
      facebook,
      linkedin,
      instagram
  } = formData;


  const formChangeHandler = (e) =>{
    setformData({...formData,[e.target.name]:e.target.value});
  }

  const formSubmitHandler = (e) =>{
    e.preventDefault();
    createProfile(formData,history);
  }


  return (
    <Fragment>
      <h1 className="large text-primary">Create Your profile</h1>
      <p className="lead">
        <i className="fas fa-user"></i> Let's get some information to make youre
        profile stand out
      </p>
      <small>* = require field</small>
      <form className="form" onSubmit={formSubmitHandler}>
        <div className="form-group">
          <select name="status" value={status} onChange={formChangeHandler}>
            <option value="0">* Select Professional Status</option>
            <option value="Devloper">Devloper</option>
            <option value="Junior Developer">Junior Developer</option>
            <option value="Senior Developer">Senior Developer</option>
            <option value="Manager">Manager</option>
            <option value="Student or Learning">Student or Learning</option>
            <option value="Instructor">Instructor or Teacher</option>
            <option value="Intern">Intern</option>
            <option value="Other">Other</option>
          </select>
          <small className="form-text">
            Give us an idea of ther you are at in your career
          </small>
        </div>
        <div className="form-group">
          <input type="text" placeholder="Company" name="company" value={company} onChange={formChangeHandler} />
          <small className="form-text">
            Cloud be your own or a company or one you work for
          </small>
        </div>
        <div className="form-group">
          <input type="text" placeholder="website" name="website" value={website} onChange={formChangeHandler}/>
          <small className="form-text">
            Cloud be your own or a company website
          </small>
        </div>
        <div className="form-group">
          <input type="text" placeholder="Location" name="location" value={location} onChange={formChangeHandler}/>
          <small className="form-text">
            City & state suggested (eg.Bhavnagar,Gujarat)
          </small>
        </div>
        <div className="form-group">
          <input type="text" placeholder="* Skills" name="skills" value={skills} onChange={formChangeHandler}/>
          <small className="form-text">
            Please use comma separated values (eg. HTML,CSS,Javascript,PHP)
          </small>
        </div>
        <div className="form-group">
          <input
            type="text"
            placeholder="Github Username"
            name="githubusername"
            value={githubusername}
            onChange={formChangeHandler}
          />
          <small className="form-text">
            If you want your latest repos and a Giyhub link, include your
            username
          </small>
        </div>
        <div className="form-group">
          <textarea placeholder="A short bio of yourself" name="bio" value={bio} onChange={formChangeHandler}></textarea>
          <small className="form-text">Tell us a littel about yourself</small>
        </div>
        <div className="my-2">
          <button
            onClick={() => toggleSocialInputs(!displaySocialInputs)}
            type="button"
            className="btn btn-light"
          >
            Add Social Network Links
          </button>
          <span>option</span>
        </div>
        {displaySocialInputs && (
          <Fragment>
            <div className="form-group social-input">
              <i className="fab fa-youtube fa-2x"></i>
              <input type="text" placeholder="Youtube URL" name="youtube" value={youtube} onChange={formChangeHandler}/>
            </div>
            <div className="form-group social-input">
              <i className="fab fa-twitter fa-2x"></i>
              <input type="text" placeholder="Twitter URL" name="twitter" value={twitter} onChange={formChangeHandler}/>
            </div>
            <div className="form-group social-input">
              <i className="fab fa-facebook fa-2x"></i>
              <input type="text" placeholder="Facebook URL" name="facebook" value={facebook} onChange={formChangeHandler}/>
            </div>
            <div className="form-group social-input">
              <i className="fab fa-linkedin fa-2x"></i>
              <input type="text" placeholder="Linkedin URL" name="linkedin" value={linkedin} onChange={formChangeHandler}/>
            </div>
            <div className="form-group social-input">
              <i className="fab fa-instagram fa-2x"></i>
              <input type="text" placeholder="Instagram URL" name="instagram" value={instagram} onChange={formChangeHandler}/>
            </div>
          </Fragment>
        )}

        <input type="submit" className="btn btn-primary my-1" />
        <Link className="btn btn-light my-1" to="/dashboard">
          Go Back
        </Link>
      </form>
    </Fragment>
  );
}

CreateProfile.propTypes = {
  createProfile:PropTypes.func.isRequired
};



export default connect(null,{createProfile})(withRouter(CreateProfile));

