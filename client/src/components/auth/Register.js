import React, {Fragment,useState } from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import { setAlert } from '../../actions/alert';
import { register } from '../../actions/auth';
import PropTypes from 'prop-types';


// import axios from "axios";  

const Register = ({ setAlert,register }) => {

    const [formData, setformData] = useState(
        {
            name:"",
            email:"",
            password:"",
            password2:""
        }
    );

        const {name,email,password,password2} = formData;

        const handelChange = (e) => setformData({...formData,[e.target.name]:e.target.value});

        const submitChangeHandler = async(e) =>{
            e.preventDefault();
            if(password !== password2){
                setAlert("password is not match","danger");
            }else{
              
              register({name,email,password});

                // console.log("Success");

                // const newuser ={
                //     name,
                //     email,
                //     password
                // }


                // try{
                //     const config ={
                //         headers:{
                //             'Content-Type':"application/json"
                //         }
                //     };

                //     const body = JSON.stringify(newuser);

                //     const res = await axios.post("/api/users",body,config);

                //     console.log(res.data);

                // }catch(err){
                //     console.error(err.response.data);
                // }
                
            } 
            
        }

  return (
    <Fragment>
      <h1 className="large text-primary">Sign Up</h1>
      <p className="lead">
        <i className="fas fa-user"></i>Create Your Account
      </p>
      <form className="form" onSubmit={submitChangeHandler}>
        <div className="form-group">
          <input 
          type="text" 
          placeholder="Name" 
          name="name" 
          value={name}
          onChange={handelChange}
          />
        </div>
        <div className="form-group">
          <input
            type="email"
            placeholder="Email Address"
            name="email"
            value={email}
          onChange={handelChange}
       
          />
        </div>
        <div className="form-group">
          <small className="form-text">
            This site uses Gravatar so is you want a profile image, use a
            Gravatar email
          </small>
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Password"
            name="password"
            value={password}
          onChange={handelChange}
          />
        </div>
        <div className="form-group">
          <input
            type="password"
            placeholder="Comfirm Password"
            name="password2"
            value={password2}
          onChange={handelChange}
          />
        </div>
        <input type="submit" className="btn btn-primary" value="Register" />
      </form>
      <p className="my-1">
        Aplready have an Account ? <Link to="/login">Sign in</Link>
      </p>
    </Fragment>
  );
};

Register.propTypes = {
    
    setAlert:PropTypes.func.isRequired,
    register:PropTypes.func.isRequired

};

export default connect(null,{setAlert,register})(Register);
