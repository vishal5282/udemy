import React, {Fragment,useState } from 'react';
import {Link, Redirect} from "react-router-dom";
// import axios from "axios";  
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {login} from "../../actions/auth";

const Login = ({ login,isAuthenticated}) => {

    const [formData, setformData] = useState(
        {
            email:"",
            password:""
        }
    );

        const {email,password} = formData;

        const handelChange = (e) => setformData({...formData,[e.target.name]:e.target.value});

        const submitChangeHandler = async(e) =>{
            e.preventDefault();

                login(email,password);

                // const newuser ={
                //     name,
                //     email,
                //     password
                // }


                // try{
                //     const config ={
                //         headers:{
                //             'Content-Type':"application/json"
                //         }
                //     };

                //     const body = JSON.stringify(newuser);

                //     const res = await axios.post("/api/users",body,config);

                //     console.log(res.data);

                // }catch(err){
                //     console.error(err.response.data);
                // }
                
            } 

  // Redirect if logged in

  if(isAuthenticated){
    return <Redirect to="/dashboard"/>
  }

  return (
    <Fragment>
      <h1 className="large text-primary">Sign In</h1>
      <p className="lead">
        <i className="fas fa-user"></i>Sign Into Your Account
      </p>
      <form className="form" onSubmit={submitChangeHandler}>
        <div className="form-group">
          <input
            type="email"
            placeholder="Email Address"
            name="email"
            value={email}
          onChange={handelChange}
          />
        </div>
        <div className="form-group">
          <input type="password" placeholder="Password" name="password" value={password} onChange={handelChange} />
        </div>
        <input type="submit" className="btn btn-primary" value="Login" />
      </form>
      <p className="my-1">
       Don't have an Account ? <Link to="/Register">Register</Link>
      </p>
    </Fragment>
  );
};

Login.propTypes = {
  login:PropTypes.func.isRequired,
  isAuthenticated:PropTypes.bool  
};


const isAuthenticated = state =>({
  isAuthenticated:state.auth.isAuthenticated
})

export default connect(isAuthenticated,{login})(Login);
