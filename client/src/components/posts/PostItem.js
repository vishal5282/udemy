import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import {addLike,removeLike} from "../../actions/post";

const PostItem = ({auth,addLike,removeLike,post:{
    _id,text,name,avatar,user,likes,comments,date
}}) =>
{
return (
    <>
    {console.log(_id)};
    <div className="post bg-white p-1 my-1">
        <div>
            <a href="index.html">
                <img className="round-img" src={avatar} alt="img" />
                <h4>{name}</h4>
            </a>
        </div>
        <div>
        <p className="my-1">
            {text}
        </p>
        <p className="post-date">Posted On <Moment format="YYYY/MM/DD">{date}</Moment></p>
        <button onClick={e => addLike(_id)} type="button" className="btn btn-light">
            <i className="fas fa-thumbs-up"></i>
            <span>{' '}{
            comments.length > 0 && <span>{comments.length}</span> 
        }</span>
        </button>
        <button onClick={e => removeLike(_id)} type="button" className="btn btn-light">
            <i className="fas fa-thumbs-down"></i>
            {' '}<span>{likes.length}</span>
        </button>
        <Link to={`/post/${_id}`} className="btn btn-primary">
        Discussion{' '}{
            comments.length > 0 && (
                <span className="comment-count">{comments.length}</span>
            )
        }
        </Link>
        {!auth.loading && auth === auth.user_id && (
            <buttn type="button" className="btn btn-danger">
        <i className="fas fa-times"></i>
        </buttn>

        )}
       
        </div>
    </div>
    </>

);
        }


PostItem.propTypes = {

}

const mapStateToProps = state =>({
    auth:state.auth

});

export default connect(mapStateToProps,{addLike,removeLike})(PostItem);
