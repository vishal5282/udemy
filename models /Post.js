const mongoose = require("mongoose");

// Post Create Schema for post


const postSchema = new mongoose.Schema({
    user:{
        type:mongoose.Schema.Types.ObjectId,
        ref:"users"
    },
    text:{
        type:String,
        require:true
    },
    name:{
        type:String,
    },
    avatar:{
        type:String
    },
    likes:[
        {
            user:{
                type:mongoose.Schema.Types.ObjectId,
                ref:"users"
            }
        }
    ],
    comments:[
        {
            user:{
                type:mongoose.Schema.Types.ObjectId,
                ref:"users"
            },
            text:{
                type:String,
                require:true
            },
            name:{
                type:String
            },
            avatar:{
                type:String
            },
            date:{
                type:Date,
                default:Date.now
            }
        }
    ],
    date:{
        type:Date,
        default:Date.now
    }
});

const Post = mongoose.model("post",postSchema);

module.exports = Post;
