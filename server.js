const express = require("express");
const app = express();
const chalk = require("chalk");
const dotenv = require("dotenv");
const connectDB = require('./config/db');
// const path = require("path");

// const { createProxyMiddleware } = require('http-proxy-middleware');

dotenv.config({path:"./config.env"});

// Connect mongoose

connectDB();

// app.use(
//     '/api',
//     createProxyMiddleware({
//       changeOrigin: true,
//     })
//   );

const PORT=process.env.PORT || 8000;

app.get('/',(req,res)=> res.send("API Running"));

// Init Middlware

app.use(express.json());

// Define routes

app.use('/api/users', require('./routers/api/users'));
app.use('/api/profile', require('./routers/api/profile'));
app.use('/api/posts', require('./routers/api/posts'));
app.use('/api/auth', require('./routers/api/auth'));



app.listen(PORT,()=>console.log(chalk.bgGreen.black`Server is running ${PORT}`));