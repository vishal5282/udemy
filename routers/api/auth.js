const express = require("express");
const route =express.Router();
const auth = require('../../middleware/auth');
const User = require("../../models /User");
const jwt = require("jsonwebtoken");
const {check, validationResult} = require("express-validator");
const config =require("config");
const bcrypt = require("bcryptjs");



// @route GEt api/auth

// @desc  test route

// @acces  public



route.get('/',auth, async(req,res)=>{

    try{

    const user = await User.findById(req.user.id).select('-password');
    res.json(user);

    }catch(err){  
    console.log(err.message);
    res.status(500).send("Server Error");
  }}
     // res.send("Auth router")
);


// @route Post api/auth

// @desc  authantication user & get token

// @acces  private


route.post(
    "/",
    [
      check("email","Please include a valid Email").isEmail(),
      check("password","password is require").exists()
  ],
    async(req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
      }
  
      const {email,password} =req.body;
  
      try{
          // See the user is Exists
          let user = await User.findOne({email});
  
          if(!user){
              return res.status(400).json({errors:[{msg:"User Invalid"}]});
          }

          const isMatch = await bcrypt.compare(password,user.password);

          if(!isMatch){
            return res.status(400).json({errors:[{msg:"Password Invalid"}]});
              
          }



          // Return Json webToken
  
          const payLoad = {
              user:{
                  id:user.id
              }
          }
          jwt.sign(payLoad,
              config.get('jwtSecret'),
              {expiresIn:36000000000},
              (err,token)=>{
                  if(err) throw err;
                  res.json({token});
  
              });
          // res.send("User route");
  
          // res.send("User Registered");
  
      }catch(err){
          console.error(err.message);
          res.status(500).send("Server Error");
  
      }
    }
  );

module.exports=route;