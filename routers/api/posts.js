const express = require("express");

const route =express.Router();

const {check,validationResult} = require("express-validator");

const auth = require("../../middleware/auth");


const Post = require("../../models /Post");

const Profile = require("../../models /Profile");

const User = require("../../models /User");






// @route Post api/post

// @desc  Create a post

// @acces  private



route.post(
  "/",
  [
    auth,
    [
        check("text", "text is require").not().notEmpty()
    ]
   ],
  async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const user = await User.findById(req.user.id).select("-password");

      const newPost = new Post({
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
      });

      const post = await newPost.save();
      res.json(post);
    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);

// @route GET api/post

// @desc  Get all posts

// @acces  private



route.get('/',auth,async(req,res)=>{

    try{

        const posts = await  Post.find().sort({date:-1});
        res.json(posts);

    }catch(err){
        console.error(err.message);
        res.status(500).send("Server Error");
    }
});

// @route GET api/posts/:id

// @desc  Get post by user ID

// @acces  Private



route.get('/:id',auth,async(req,res)=>{
    try{

        const post = await  Post.findById(req.params.id);
       
        if(!post){
            return res.status(404).json({msg:"Post no found"});
        }
        res.json(post);
    }catch(err){
        console.error(err.message);
        if(err.kind === "ObjectId"){
            return res.status(404).json({msg:"Post no found"});
        }
        res.status(500).send("Server Error");
    }
});


// @route DELETE api/post/:id

// @desc  DELETE post

// @acces  private


 
route.delete('/:id',auth,async(req,res)=>{

    try{

        const post = await  Post.findById(req.params.id);
        if(!post){
          return res.status(404).json({msg:"post not found"});

        }

        // check the user
        if(post.user.toString() !== req.user.id ){
          return res.status(401).json({msg:"User not authenticate"});

        }

        await post.remove();


        res.json({msg:"Post is removed"});
        

    }catch(err){
        console.error(err.message);
        if(err.kind === "ObjectId"){
          return res.status(404).json({msg:"Post no found"});
      }
        res.status(500).send("Server Error");
    }
});


// @route PUT api/posts/like

// @desc  Like a post

// @acces  Private



route.put("/like/:id",auth,async(req,res)=>{
  
  try{

    const post = await Post.findById(req.params.id);

    // check is post is alredy liked
    if(post.likes.filter(like => like.user.toString() === req.user.id).length>0){
      return res.status(400).json({msg:"Post is already like"});
    }

    post.likes.unshift({user:req.user.id});

    await post.save();

    res.json(post.likes);

  }catch(err){
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});

// @route PUT api/posts/unlike

// @desc  unLike a post

// @acces  Private


route.put("/unlike/:id",auth,async(req,res)=>{
  
  try{

    const post = await Post.findById(req.params.id);

    // check is post is alredy liked
    if(post.likes.filter(like => like.user.toString() === req.user.id).length !== 0){

      return res.status(400).json({msg:"Post is not yet liked"});
    }

    // Get remove the indexof

    const removeIndex = post.likes
    .map(like => like.user.toString())
    .indexOf(req.user.id);

    post.likes.splice(removeIndex, 1);

    await post.save();

    res.json(post.likes);

  }catch(err){
    console.error(err.message);
    res.status(500).send("Server Error");
  }
});



// @route Post api/post/comment/:id

// @desc  Comment on a post

// @acces  private



route.post(
  "/comment/:id",
  [
    auth,
    [
        check("text", "text is require").not().notEmpty()
    ]
   ],
  async (req,res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {

      // const user = await User.findById(req.user.id).select("-password");
      const post = await Post.findById(req.params.id);



      const newComment= {
        text: req.body.text,
        user: req.user.id
        
      };

      post.comments.unshift(newComment);

      await post.save();
      res.json(post.comments);

    } catch (err) {
      console.error(err.message);
      res.status(500).send("Server Error");
    }
  }
);


// @route DELETE api/posts/comment/:id/:comment_id

// @desc  DELETE comment

// @acces  private


 
route.delete('/comment/:id/:comment_id',auth,async(req,res)=>{

  try{

      const post = await  Post.findById(req.params.id);

      // pull out comment 

      const comment =post.comments.find(comment => comment.id === req.params.comment_id);


      // Make sure comment is exists

      if(!comment){
        return res.status(404).json({msg:"post not found"});

      }

      // check user

      if(comment.user.toString() !== req.user.id ){
        return res.status(401).json({msg:"User not authenticate"});
      }

      // Get Remove index

      const removeIndex = post.comments
      .map(comment => comment.user.toString())
      .indexOf(req.user.id); 


      post.comments.splice(removeIndex,1);

      await post.save();

      res.json(post.comments);
      
  }catch(err){
      console.error(err.message);
      if(err.kind === "ObjectId"){
        return res.status(404).json({msg:"Post no found"});
    }
      res.status(500).send("Server Error");
  }
});



module.exports=route;