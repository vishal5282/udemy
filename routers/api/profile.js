const express = require("express");

const router =express.Router();
const Profile = require("../../models /Profile");
const auth =require("../../middleware/auth");


const {check,validationResult} = require("express-validator");
// const { response } = require("express");

const Post = require("../../models /Post");
const User = require("../../models /User");


// @route GEt api/profile/me

// @desc  test route

// @acces  public



router.get('/me', auth, async (req,res) => {
    try {
      const profile = await Profile.findOne({
        user: req.user.id
      }).populate('user', ['name', 'avatar']);
  
      if (!profile) {
        return res.status(400).json({ msg: 'There is no profile for this user' });
      }
  
      res.json(profile);
    } catch (err) {
      console.log(err.message);
      res.status(500).send('Server Error');
    }
  });


// @route Post api/profile

// @desc  create or update user profile

// @acces  private

router.post('/',
    [
        auth,
        [
            check("status","status is require")
            .not()
            .notEmpty(),
            check("skills","skills is require")
            .not()
            .notEmpty()
        ]
],
async(req,res) =>{
    const errors= validationResult(req);

    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()});
    }

    const {
        company,
        website,
        location,
        status,
        bio,
        githubusername,
        skills,
        youtube,
        twitter,
        facebook,
        linkedin,
        instagram
    }=req.body;

    // build profile object

    const profileFields = {};
    profileFields.user=req.user.id;
    if(company) profileFields.company = company;
    if(website) profileFields.website = website;
    if(location) profileFields.location=location;
    if(status) profileFields.status=status;
    if(bio) profileFields.bio=bio;
    if(githubusername) profileFields.githubusername=githubusername;
    if(skills){
        // console.log(12345);  
        profileFields.skills = skills.split(" ").map(skill=>skill.trim());
    }

    // Build sociail object
    profileFields.social = {};

    if(youtube) profileFields.social.youtube=youtube;
    if(twitter) profileFields.social.twitter=twitter;
    if(facebook) profileFields.social.facebook=facebook;
    if(linkedin) profileFields.social.linkedin = linkedin;
    if(instagram) profileFields.social.instagram =instagram;    

    try{

        let profile = await Profile.findOne({user:req.user.id});
      

        if(profile){
            // Update

            profile = await Profile.findOneAndUpdate(
            {user:req.user.id},
            {$set:profileFields},
            {new:true}
            );
            return res.json(profile);
        }

        // create

        profile = new Profile(profileFields);

        await profile.save();

        res.json(profile);


    }catch(err){
        console.error(err.message);
        res.status(500).send("server Error");
    }
    // console.log(profileFields.skills);
    // res.send("hello");
});


// @route Get api/profile

// @desc  Get all profiles

// @acces  public

router.get('/', async(req,res)=>{
    try{

        const profiles = await Profile.find().populate('user',['name','avatar']);

        res.json(profiles);
    }catch(err){
        console.error(err.message);
        res.status(500).send("Server Error")
    }
});


// @route Get api/profile/user/:user_id

// @desc  Get profile by user ID

// @acces  Private

router.get('/user/:user_id', async(req,res)=>{
    try{

        const profile = await Profile.findOne({user:req.params.user_id}).populate('user',['name','avatar']);

        if(!profile)
        return res.status(400).json({msg:"Profile not found"});

        res.json(profile);
    }catch(err){
        console.error(err.message);
        if(err.kind == 'ObjectId'){
        return res.status(400).json({msg:"Profile not found"});
        }
        res.status(500).send("Server Error")
    }
});


// @route DELETE api/profile

// @desc  DELETE profile,user and posts

// @acces  Private

router.delete('/',auth, async(req,res)=>{
    try{
        // Remove Post user

        await Post.deleteMany({user:req.user.id});

        // Remove profile
        await Profile.findOneAndRemove({user:req.user.id});
        // Remove user
        await User.findOneAndRemove({_id:req.user.id});

        res.json({msg:"user Delete"});
    }catch(err){
        console.error(err.message); 
        res.status(500).send("Server Error")
    }
});


// @route PUT api/profile/experience

// @desc  add profile experience

// @acces  Private

router.put('/experience',
    [
        auth,[
            check('title','title is require').not().notEmpty(),
            check('company','company is require').not().notEmpty(),
        ]
    ],
    async(req,res) =>{
        const errors = validationResult(req);


        if(!errors.isEmpty()){
            return res.status(400).json({errors:errors.array()});
        }

        const {
            title,
            company,
            location,
            from,
            to,
            current,
            description
        }=req.body;

        // create object


        const newExp = {
            title,
            company,
            location,
            from,
            to,
            current,
            description
        };

        try{

            const profile = await Profile.findOne({user:req.user.id});

            // console.log(1245);

            profile.experience.unshift(newExp);


            // console.log(Profile);

            await profile.save();

            res.json(profile);

        }catch(err){
            console.error(err.message);
            res.status(500).send("Server Error");
       }
    }
);


// @route DELETE api/profile/experience/:exp_id

// @desc  DELETE experience from profile

// @acces  Private



router.delete('/experience/:exp_id',
    auth, async(req,res) =>{
        try{

            const profile = await Profile.findOne({user:req.user.id});

            // Get the Remove index

            const removeIndex = profile.experience
            .map(item =>item.id)
            .indexOf(req.params.exp_id);


            profile.experience.splice(removeIndex,1);

            await profile.save();

            res.json(profile);

        }catch(err){
            console.error(err.message);
            res.status(500).send("Server Error");
        }

    });


// @route PUT api/profile/education

// @desc  add profile education

// @acces  Private

router.put('/education',
    [
        auth,[
            check('school','school is require').not().notEmpty(),
            check('degree','degree is require').not().notEmpty(),
            check('fieldofstudy','fieldofstudy is require').not().notEmpty(),
            check('from','from is require').not().notEmpty()

        ]
    ],
    async(req,res) =>{
        const errors = validationResult(req);


        if(!errors.isEmpty()){
            return res.status(400).json({errors:errors.array()});
        }

        const {
            school,
            degree,
            fieldofstudytion,
            from,
            to,
            current,
            description
        }=req.body;

        // create object


        const newEdu = {
            school,
            degree,
            fieldofstudytion,
            from,
            to,
            current,
            description
        };

        try{

            const profile = await Profile.findOne({user:req.user.id});

            // console.log(1245);

            profile.education.unshift(newEdu);


            // console.log(Profile);

            await profile.save();

            res.json(profile);

        }catch(err){
            console.error(err.message);
            res.status(500).send("Server Error");
       }
    }
);

// @route DELETE api/profile/education/:exp_id

// @desc  DELETE education from profile

// @acces  Private



router.delete('/education/:exp_id',
    auth, async(req,res) =>{
        try{

            const profile = await Profile.findOne({user:req.user.id});

            // Get the Remove index

            const removeIndexofeducation = profile.education
            .map(item =>item.id)
            .indexOf(req.params.exp_id);


            profile.education.splice(removeIndexofeducation,1);

            await profile.save();

            res.json(profile);

        }catch(err){
            console.error(err.message);
            res.status(500).send("Server Error");
        }

    });




    // --------------------------------------------- github part error not set path -------------------------------



// @route GET api/profile/github/:username

// @desc  Get user repos from github

// @acces  Public



// route.get("/github/:username", (req, res) => {
//   try {
//     const options = {
//       url: `https://api.github.com/users/${req.params.username}
//         /repos?per_page=5&sort=created:asc&client_id=${config.get(
//           "githubClientId"
//         )}&client_secret=${config.get("githubSecret")}`,

//       method: "GET",
//       headers: { 'User-Agent': 'request'}
//     };

//     request(options,(error,response,body) =>{
//         if(error){console.error(error);}

//         if(response.statusCode !==200) {
//             res.status(404).json({msg:chalk.bgRed.black`No github found`});
//         }

//         console.log('statusCode:', response && response.statusCode);

//         res.json(JSON.parse(body));
//     });


    // try{

    //     const options ={
            
    //         url:"https://github.com/vishal3080",

    //         headers: { 'User-Agent': 'request'}
    //     }
    // request(options,(error,response,body) =>{
    //     console.error('error:', error); // Print the error if one occurred
    //     console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
    //     // console.log('body:', body); // Print the HTML for the Google homepage.

    // })
//   } catch (err) {
//     console.error(err.message);
//     res.status(500).send("Server Error");
//   }
// });


module.exports=router;
